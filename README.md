## Description

Repository of the laboratory wark of web-programming

## Installation

Add `.env` file in server directory according to the `.env.example` file and run next command in `server` and `client` folders:
```bash
$ npm install
```

## Running the app
Run next command on `server` directory and after in the `client`:
```bash
$ npm run start
```
