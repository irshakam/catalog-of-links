import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { UserState as User } from "../store/user/user.types";

export const userApi = createApi({
  reducerPath: "userApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:3030",
  }),
  tagTypes: ["User"],
  endpoints: (build) => ({
    getUser: build.query<User, number>({
      query: (id: number) => `/users/${id}`,
      transformResponse: (response: { data: User }) => response.data,
    }),
    updateUser: build.mutation({
      query: ({ body, token, id }) => ({
        url: `/users/${id}`,
        method: "PATCH",
        body,
        headers: {
          authorization: token,
        },
      }),
    }),
  }),
});

export const { useGetUserQuery, useLazyGetUserQuery, useUpdateUserMutation } =
  userApi;
