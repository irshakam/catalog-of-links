import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const authApi = createApi({
  reducerPath: "userApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:3030/",
  }),
  tagTypes: ["User"],
  endpoints: (build) => ({
    loginUser: build.mutation({
      query: ({ email, password }: { email: string; password: string }) => ({
        url: `/login`,
        method: "POST",
        body: {
          email,
          password,
        },
      }),
    }),
    signUpUser: build.mutation({
      query: (body) => ({
        url: `/users`,
        method: "POST",
        body,
      }),
    }),
  }),
});

export const { useLoginUserMutation, useSignUpUserMutation } = authApi;
