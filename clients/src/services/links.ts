import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { RootState } from "../store";
import { UserState } from "../store/user/user.types";
import { LinkInterface } from "../store/link/link.types";

export const linksApi = createApi({
  reducerPath: "linksApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:3030",
    prepareHeaders: (headers, { getState }) => {
      const token = (getState() as RootState).user.accessToken;

      if (token) {
        headers.set("Authorization", token);
      }

      return headers;
    },
  }),
  tagTypes: ["artPost"],
  endpoints: (build) => ({
    getLinks: build.query<{ post: LinkInterface; userInfo: UserState }[], void>({
      query: () => `/links`,
      transformResponse: (response: { post: LinkInterface; userInfo: UserState }[]) =>
        response,
    }),
    getSingleLink: build.query<LinkInterface, number>({
      query: (id: number) => `/links/${id}`,
      transformResponse: (response: LinkInterface) => response,
    }),
    getLinkByUserId: build.query<LinkInterface[], number>({
      query: (userId: number) => `/links/by-user-id?userId=${userId}`,
      transformResponse: (response: LinkInterface[]) => response,
    }),
    addNewLink: build.mutation({
      query: (body) => ({
        url: `/links`,
        method: "POST",
        body,
      }),
    }),
    deleteLink: build.mutation({
      query: (id: number) => ({
        url: `/links/${id}`,
        method: "DELETE",
      }),
    }),
  }),
});

export const {
  useGetSingleLinkQuery,
  useGetLinksQuery,
  useLazyGetLinksQuery,
  useGetLinkByUserIdQuery,
  useAddNewLinkMutation,
  useDeleteLinkMutation,
} = linksApi;
