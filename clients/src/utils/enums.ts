export enum PathName {
  Login = "/login",
  SignUp = "/signup",
  Links = "/links",
  LinksList = "/links/list",
  NewLink = "/links/new-link",
  MyProfile = "/my-profile",
}
