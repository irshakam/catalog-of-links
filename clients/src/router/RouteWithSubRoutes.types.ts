import { PathName } from "../utils/enums";

export interface RouteWithSubRoutesProps {
  path: PathName;
  component: React.FC<any>;
  exact: boolean;
  redirectLoggedUser?: true;
  private: boolean;
  fallbackRedirect?: PathName;
  routes?: RouteWithSubRoutesProps[];
}

export interface RouterChild {
  path: PathName;
  component: React.FC;
  exact: boolean;
  private: boolean;
}
