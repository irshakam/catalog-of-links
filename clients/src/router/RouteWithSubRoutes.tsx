import React from "react";
import { Redirect, Route } from "react-router-dom";
import { RouteWithSubRoutesProps } from "./RouteWithSubRoutes.types";
import { useAppSelector } from "../hooks/redux";
import { PathName } from "../utils/enums";

const RouteWithSubRoutes: React.FC<RouteWithSubRoutesProps> = (route) => {
  const isAuth: boolean = !!useAppSelector((state) => state.user.accessToken);

  return (
    <>
      <Route
        path={route.path}
        render={(props) =>
          route.private ? (
            isAuth ? (
              <route.component {...props} routes={route.routes} />
            ) : (
              <Redirect to={PathName.Login} />
            )
          ) : (
            <route.component {...props} routes={route.routes} />
          )
        }
      />
    </>
  );
};

export default RouteWithSubRoutes;
