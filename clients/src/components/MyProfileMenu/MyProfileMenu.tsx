import { motion } from "framer-motion";
import { Link } from "react-router-dom";
import { PathName } from "../../utils/enums";
import { useAppDispatch } from "../../hooks/redux";
import { clearUser } from "../../store/user/userSlice";

const MyProfileMenu = () => {
  const dispatch = useAppDispatch();
  const logOut = () => {
    dispatch(clearUser());
  };

  return (
    <motion.div
      transition={{ duration: 0.5 }}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <div className="min-w-max bg-blue-900 absolute top-12 right-0 p-5 text-white rounded-lg backdrop-opacity-50">
        <Link to={PathName.MyProfile}>
          <h4 className="flex gap-1">
            <p className="text-sky-500 px-px">{">"}</p>
            My Profile
          </h4>
        </Link>
        <Link to={PathName.Login}>
          <h4 className="flex gap-1" onClick={logOut}>
            <p className="text-sky-500 px-px">{">"}</p>
            Log out
          </h4>
        </Link>
      </div>
    </motion.div>
  );
};

export default MyProfileMenu;
