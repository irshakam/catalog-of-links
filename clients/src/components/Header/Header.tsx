import star from "../../assets/star.png";
import { ReactComponent as Avatar } from "../../assets/avatar.svg";
import { AnimatePresence } from "framer-motion";
import MyProfileMenu from "../MyProfileMenu";
import { useRef, useState } from "react";
import { useClickAway } from "react-use";
import { Link } from "react-router-dom";
import { PathName } from "../../utils/enums";
import { useAppSelector } from "../../hooks/redux";

const Header = () => {
  const isAuth: boolean = !!useAppSelector((state) => state.user.accessToken);
  const [showMenu, setShowMenu] = useState(false);
  const refMenu = useRef(null);
  const profileIconId = "profileIcon";

  useClickAway(refMenu, (e) => {
    if ((e.target as HTMLButtonElement).parentElement?.id !== profileIconId) {
      setShowMenu(false);
    }
  });

  return (
    <header className="w-full h-10 flex justify-between items-center px-4 bg-blue-900 lg:h-12 xl:px-8">
      <Link to={PathName.LinksList}>
        <div className="font-mono text-xs italic font-light text-sky-200">
          <div className="flex gap-1">
            <h1>Catalog</h1>
            <img src={star} alt="star" className="w-3 h-3" />
          </div>
          <div className="pl-1 flex gap-1">
            <img src={star} alt="star" className="w-3 h-3" />
            <h1>of links</h1>
          </div>
        </div>
      </Link>
      {isAuth ? (
        <div className="relative">
          <Avatar
            fill="#f0f9ff"
            id={profileIconId}
            className="w-5 max-h-5 border rounded-lg border-sky-200 sm:w-6 sm:max-h-6"
            onClick={() => setShowMenu(!showMenu)}
          />
          <AnimatePresence>
            {showMenu && (
              <div ref={refMenu}>
                <MyProfileMenu />
              </div>
            )}
          </AnimatePresence>
        </div>
      ) : (
        <Link to={PathName.Login}>
          <div className="font-mono text-xs italic font-light text-sky-200">
            Log in
          </div>
        </Link>
      )}
    </header>
  );
};

export default Header;
