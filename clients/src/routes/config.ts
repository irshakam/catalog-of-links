import { PathName } from "../utils/enums";
import Login from "../pages/Login";
import SignUp from "../pages/SignUp";
import MyProfile from "../pages/MyProfile";
import Links from "../pages/Links";
import { RouteWithSubRoutesProps } from "../router/RouteWithSubRoutes.types";
import LinksList from "../pages/Links/LinksList";
import CreateLink from "../pages/Links/CreateLink";

export const routes: RouteWithSubRoutesProps[] = [
  {
    path: PathName.Login,
    component: Login,
    exact: true,
    redirectLoggedUser: true,
    private: false,
    fallbackRedirect: PathName.MyProfile,
  },
  {
    path: PathName.SignUp,
    component: SignUp,
    exact: true,
    redirectLoggedUser: true,
    private: false,
    fallbackRedirect: PathName.MyProfile,
  },
  {
    path: PathName.MyProfile,
    component: MyProfile,
    exact: true,
    private: true,
  },

  {
    path: PathName.Links,
    component: Links,
    exact: false,
    private: false,
    routes: [
        {
            path: PathName.LinksList,
            component: LinksList,
            exact: true,
            private: false,
        },
        {
            path: PathName.NewLink,
            component: CreateLink,
            exact: true,
            private: true,
        },
    ]

  },
];
