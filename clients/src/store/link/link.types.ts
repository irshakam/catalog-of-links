export interface LinkInterface {
  id: number;
  userId: number;
  userName: string;
  link: string;
  about: string;
  tag: string;
  isActive: boolean;
  createdAt: Date;
  updatedAt: Date;
}
