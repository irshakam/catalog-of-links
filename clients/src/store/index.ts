import {
  AnyAction,
  combineReducers,
  configureStore,
  getDefaultMiddleware,
  Reducer,
} from "@reduxjs/toolkit";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import { authApi } from "../services/auth";
import { userApi } from "../services/user";
import { linksApi } from "../services/links";

import userReducer from "./user/userSlice";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["user", "signUp"],
};

const middlewares = [
  authApi.middleware,
  userApi.middleware,
  linksApi.middleware,
];

const appReducer = combineReducers({
  user: userReducer,
  [authApi.reducerPath]: authApi.reducer,
  [userApi.reducerPath]: userApi.reducer,
  [linksApi.reducerPath]: linksApi.reducer,
});

const rootReducer: Reducer = (state: RootState, action: AnyAction) => {
  if (action.type === "user/clearUser") {
    storage.removeItem("persist:root");
    state = {} as RootState;
  }
  return appReducer(state, action);
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const middleware = getDefaultMiddleware({
  serializableCheck: false,
}).concat(middlewares);

export const store = configureStore({
  reducer: persistedReducer,
  middleware,
});

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof appReducer>;
export type AppDispatch = typeof store.dispatch;
