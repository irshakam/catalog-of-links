export interface UserState {
  id: number;
  email: string;
  name: string;
  accessToken: string;
}
