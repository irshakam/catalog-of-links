import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { UserState } from "./user.types";

const initialState: UserState = {
  id: 0,
  email: "",
  name: "",
  accessToken: "",
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<UserState>) => {
      return {
        ...state,
        ...action.payload,
      };
    },
    clearUser: (): UserState => initialState,
  },
});

export const { setUser, clearUser } = userSlice.actions;

export default userSlice.reducer;
