import { useEffect } from "react";
import { PathName } from "../../../utils/enums";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { useAddNewLinkMutation } from "../../../services/links";
import { useAppSelector } from "../../../hooks/redux";

const isUrlExist = (url: string) => {
  var request = new XMLHttpRequest();
  request.open("GET", url, true);
  request.onreadystatechange = function () {
    if (request.readyState === 4) {
      if (request.status === 404) {
        return false;
      }
    }
  };
  return true;
};

const CreateLink = () => {
  const user = useAppSelector((state) => state.user);
  const history = useHistory();
  const [createLink, { data, isSuccess, isError }] = useAddNewLinkMutation();
  const { register, handleSubmit } = useForm({
    shouldUseNativeValidation: true,
  });

  const onSubmit = async (data: { [name: string]: string }) => {
    createLink({...data, userId: user.id, userName: user.name, isActive: isUrlExist(data?.link)});
  };

  useEffect(() => {
    if (isSuccess) {
      alert("Success create link!");
      history.push(PathName.LinksList);
    }

    if (isError) {
      alert("Something went wrong...");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, isSuccess, isError]);

  return (
    <div className="border border-blue-900 rounded-lg mt-16 w-11/12 p-1 bg-blue-900 bg-opacity-30 md:w-2/3 mx-auto lg:w-1/3">
      <h1 className="w-full text-center text-blue-900 mb-4">Create new link</h1>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="flex flex-col items-center justify-center gap-4"
      >
        <label className="w-full flex flex-col justify-center lg:flex-row gap-4">
          <p className="w-16 text-blue-900 md:w-32">link: </p>
          <input
            className="border border-blue-900 rounded-lg bg-slate-200"
            {...register("link", {
              required: "Please enter your link.",
              pattern:
                /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/,
            })}
          />
        </label>
        <label className="w-full flex flex-col justify-center lg:flex-row gap-4">
          <p className="w-16 text-blue-900 md:w-32">About: </p>
          <input
            className="border border-blue-900 rounded-lg bg-slate-200"
            {...register("about", {
              required: "Please fill about field.",
            })}
          />
        </label>
        <label className="w-full flex flex-col justify-center lg:flex-row gap-4">
          <p className="w-16 text-blue-900 md:w-32">Tags: </p>
          <input
            className="border border-blue-900 rounded-lg bg-slate-200"
            {...register("tag", {
              required: "Please enter tag.",
            })}
          />
        </label>
        <p className="w-full text-center text-xs text-blue-900">
          *Please enter tags through a comma
        </p>
        <button
          type="submit"
          className="m-auto w-max text-blue-900 bg-slate-200 p-2 rounded-lg"
        >
          Create
        </button>
      </form>
    </div>
  );
};

export default CreateLink;
