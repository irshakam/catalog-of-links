import { Switch } from "react-router-dom";
import RouteWithSubRoutes from "../../router/RouteWithSubRoutes";
import { RouteWithSubRoutesProps } from "../../router/RouteWithSubRoutes.types";

const Links: React.FC<RouteWithSubRoutesProps> = ({ routes }) => {
  return (
    <div
      className="text-sm bg-slate-200 sm:text-base"
      style={{ minHeight: "93vh" }}
    >
      <Switch>
        {routes?.map((route, i) => (
          <RouteWithSubRoutes key={i} {...route} />
        ))}
      </Switch>
    </div>
  );
};

export default Links;
