import { useEffect } from "react";
import { useAppSelector } from "../../../hooks/redux";
import {
  useDeleteLinkMutation,
  useLazyGetLinksQuery,
} from "../../../services/links";

const LinksList = () => {
  const [getLinks, { data }] = useLazyGetLinksQuery();
  const isAuth: boolean = !!useAppSelector((state) => state.user.accessToken);
  const [deleteLink] = useDeleteLinkMutation();

  const onDelete = (id: number) => () => {
    deleteLink(id)
      .unwrap()
      .then(() => {
        alert("Success create link!");
        getLinks();
      })
      .catch(() => alert("Something went wrong..."));
  };

  useEffect(() => {
    getLinks();
  }, []);

  return (
    <div className="w-full m-auto my-4 p-5 md:w-2/3 lg:w-1/3">
      {data?.map(({ post, userInfo }) => {
        return (
          <div
            key={post.id}
            className="border border-sky-200 bg-white rounded p-5"
          >
            <h2>Author: @{userInfo.name}</h2>
            <h1 className="font-bold text-blue-900 text-lg">
              Link: {post.link}
            </h1>
            <h1 className="font-mono text-blue-700 text-lg">
              About: {post.about}
            </h1>
            <h1 className="font-mono text-lg">Tags: {post.tag}</h1>
            <h1 className="font-mono text-lg">
              Status: {post.isActive ? "Active" : "Archive"}
            </h1>

            {isAuth && (
              <button
                className="mt-4 text-sm"
                onClick={onDelete(post.id)}
                style={{ color: "#991b1b" }}
              >
                Delete
              </button>
            )}
          </div>
        );
      })}
    </div>
  );
};

export default LinksList;
