import { useAppSelector } from "../../hooks/redux";
import { useEffect } from "react";
import { PathName } from "../../utils/enums";
import { Link, useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { useSignUpUserMutation } from "../../services/auth";

const SignUp = () => {
  const isAuth: boolean = !!useAppSelector((state) => state.user.accessToken);
  const history = useHistory();
  const [signup, { data, isSuccess, isError }] = useSignUpUserMutation();
  const { register, handleSubmit } = useForm({
    shouldUseNativeValidation: true,
  });

  const onSubmit = (data: { [name: string]: string }) => {
    signup(data);
  };

  useEffect(() => {
    if (isSuccess) {
      alert("Success create account!");
      history.push(PathName.LinksList);
    }

    if (isError) {
      alert("Something went wrong...");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, isSuccess, isError]);

  useEffect(() => {
    if (isAuth) {
      history.push(PathName.LinksList);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAuth]);

  return (
    <div className="border border-blue-900 rounded-lg mt-16 w-11/12 p-1 bg-blue-900 bg-opacity-30 md:w-2/3 mx-auto lg:w-1/3">
      <h1 className="w-full text-center text-blue-900 mb-4">Sign Up</h1>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="flex flex-col items-center justify-center gap-4"
      >
        <label className="w-full flex flex-col justify-center lg:flex-row gap-4">
          <p className="w-16 text-blue-900 md:w-32">Email: </p>
          <input
            className="border border-blue-900 rounded-lg bg-slate-200"
            {...register("email", {
              required: "Please enter your email.",
              pattern: /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/,
            })}
          />
        </label>
        <label className="w-full flex flex-col justify-center lg:flex-row gap-4">
          <p className="w-16 text-blue-900 md:w-32">Password: </p>
          <input
            type="password"
            className="border border-blue-900 rounded-lg bg-slate-200"
            {...register("password", {
              required: "Please enter your password.",
              pattern: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/,
            })}
          />
        </label>
        <label className="w-full flex flex-col justify-center lg:flex-row gap-4">
          <p className="w-16 text-blue-900 md:w-32">Name: </p>
          <input
            className="border border-blue-900 rounded-lg bg-slate-200"
            {...register("name", {
              required: "Please enter your name.",
            })}
          />
        </label>
        <button
          type="submit"
          className="m-auto w-max text-blue-900 bg-slate-200 p-2 rounded-lg"
        >
          Sign Up
        </button>
        <Link
          to={PathName.Login}
          className="text-sm text-white underline text-center"
        >
          Already have an account? Log In...
        </Link>
      </form>
    </div>
  );
};

export default SignUp;
