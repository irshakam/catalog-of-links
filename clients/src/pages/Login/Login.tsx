import { useLoginUserMutation } from "../../services/auth";
import { useEffect } from "react";
import { useHistory, Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks/redux";
import { setUser } from "../../store/user/userSlice";
import { PathName } from "../../utils/enums";
import { useForm } from "react-hook-form";

const Login = () => {
  const [login, { data }] = useLoginUserMutation();
  const dispatch = useAppDispatch();
  const history = useHistory();
  const isAuth: boolean = !!useAppSelector((state) => state.user.accessToken);
  const { register, handleSubmit } = useForm({
    shouldUseNativeValidation: true,
  });

  const onSubmit = (data: { [name: string]: string }) => {
    login({ email: data.email, password: data.password });
  };

  useEffect(() => {
    if (data) {
      dispatch(
        setUser({
          id: data.user.id,
          email: data.user.email,
          name: data.user.name,
          accessToken: data.token,
        })
      );
      history.push(PathName.LinksList);
    }
  }, [data]);

  useEffect(() => {
    if (isAuth) {
      history.push(PathName.LinksList);
    }
  }, [isAuth]);

  return (
    <div className="border border-blue-900 rounded-lg mt-16 w-11/12 p-1 bg-blue-900 bg-opacity-30 md:w-2/3 mx-auto lg:w-1/3">
      <h1 className="w-full text-center text-blue-900 mb-4">Log In</h1>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="flex flex-col items-center justify-center gap-4"
      >
        <label className="w-full flex flex-col justify-center lg:flex-row gap-4">
          <p className="w-16 text-blue-900 md:w-32">Email: </p>
          <input
            className="border border-blue-900 rounded-lg bg-slate-200"
            {...register("email", {
              required: "Please enter your email.",
              pattern: /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/,
            })}
          />
        </label>
        <label className="w-full flex flex-col justify-center lg:flex-row gap-4">
          <p className="w-16 text-blue-900 md:w-32">Password: </p>
          <input
            type="password"
            className="border border-blue-900 rounded-lg bg-slate-200"
            {...register("password", {
              required: "Please enter your password.",
              pattern: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/,
            })}
          />
        </label>
        <button
          type="submit"
          className="m-auto w-max text-blue-900 bg-slate-200 p-2 rounded-lg"
        >
          Log In
        </button>
        <Link
          to={PathName.SignUp}
          className="text-sm text-white underline text-center"
        >
          Still don't have an account? Register...
        </Link>
      </form>
    </div>
  );
};

export default Login;
