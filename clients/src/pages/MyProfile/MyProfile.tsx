import { Link } from "react-router-dom";
import { useAppSelector } from "../../hooks/redux";
import { PathName } from "../../utils/enums";

const MyProfile = () => {
  const user = useAppSelector((state) => state.user);
  return (
    <div className="mt-12 flex flex-col items-center">
      <div className="self-end">
      <Link to={PathName.NewLink}>
        <button
          className="text-xl mx-12 p-2 text-white bg-blue-900 rounded-full"
          title={"Add new link..."}
        >
          +
        </button>
      </Link>
      </div>
      <div className="w-11/12 mx-auto border border-blue-900 p-2 lg:w-1/2">
        <h2
          className="underline decoratio
            n-sky-500 text-center text-xl"
        >
          {user.name}
        </h2>
      </div>
      <div className="flex flex-row gap-4">
        <div className="h-12 w-px bg-sky-500" />
        <div>
          <p>Email:</p>
          <p className="text-right">{user.email}</p>
        </div>
      </div>
    </div>
  );
};

export default MyProfile;
