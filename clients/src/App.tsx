import { routes } from "./routes/config";
import { BrowserRouter, Switch, Redirect } from "react-router-dom";
import RouteWithSubRoutes from "./router/RouteWithSubRoutes";
import Header from "./components/Header";
import { PathName } from "./utils/enums";

function App() {
  return (
    <BrowserRouter>
      <div className="w-full">
        <Header />

        <Switch>
          {routes.map((route, index) => (
            <RouteWithSubRoutes key={index} {...route} />
          ))}
          <Redirect exact from="/" to={PathName.LinksList} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
