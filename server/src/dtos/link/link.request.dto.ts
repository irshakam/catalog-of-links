import { IsBoolean, IsNotEmpty, IsNumber, IsString, Length } from "class-validator";

export class LinkRequestDto {
  @IsString()
  @IsNotEmpty()
  readonly userId: number;

  @IsString()
  @IsNotEmpty()
  readonly userName: string;

  @IsString()
  @IsNotEmpty()
  readonly link: string;

  @IsString()
  @IsNotEmpty()
  @Length(8, 100, { message: "name must be between 8 and 100 characters" })
  readonly about: string;

  @IsString()
  @IsNotEmpty()
  @Length(8, 100, { message: "name must be between 8 and 100 characters" })
  readonly tag: string;

  @IsBoolean()
  @IsNotEmpty()
  readonly isActive: boolean;
}
