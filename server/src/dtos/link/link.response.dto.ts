import { LinkInterface } from "@entities/link/interfaces/link.interface";

export class LinkResponseDto {
  readonly id: number;

  readonly about: string;

  readonly link: string;

  readonly userName: string;

  readonly userId: number;

  readonly tag: string;

  readonly isActive: boolean;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  constructor(data?: Partial<LinkInterface>) {
    if (data) {
      this.id = data.id;
      this.userId = data.userId;
      this.userName = data.userName;
      this.link = data.link;
      this.about = data.about;
      this.tag = data.tag;
      this.isActive = data.isActive;
      this.createdAt = data.createdAt;
      this.updatedAt = data.updatedAt;
    }
  }
}
