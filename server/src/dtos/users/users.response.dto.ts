import { UserInterface } from "@entities/user/interfaces/user.interface";

export class UsersResponseDto {
  readonly id: number;

  readonly name: string;

  readonly email: string;

  readonly password: string;

  constructor(data?: Partial<UserInterface>) {
    if (data) {
      this.id = data.id;
      this.name = data.name;
      this.email = data.email;
      this.password = data.password;
    }
  }
}
