import { IsNotEmpty, IsString, Length } from "class-validator";

export class UsersRequestDto {
  @IsString()
  @IsNotEmpty()
  @Length(1, 100, { message: "name must be between 1 and 100 characters" })
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @Length(1, 100, { message: "email must be between 1 and 100 characters" })
  readonly email: string;

  @IsString()
  @IsNotEmpty()
  @Length(8, 100, { message: "email must be between 8 and 100 characters" })
  readonly password: string;
}
