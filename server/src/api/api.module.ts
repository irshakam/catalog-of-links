import { Module } from "@nestjs/common";

import { UsersApiModule } from "./users/users.module";
import { AuthApiModule } from "./auth/auth.module";
import { LinksApiModule } from "./links/links.module";

@Module({
  imports: [
    UsersApiModule,
    AuthApiModule,
    LinksApiModule,
  ],
})
export class ApiModule {}
