import {
  Get,
  Post,
  Body,
  Param,
  Query,
  Delete,
  HttpCode,
  Controller,
  BadRequestException,
  NotFoundException,
  Put,
  UseGuards,
} from "@nestjs/common";

import { LinkService } from "@entities/link/link.service";
import { UsersService } from "@entities/user/users.service";

import { LinkRequestDto, LinkResponseDto } from "@dtos/link";
import { AuthGuard } from "@common/guard/authGuard";
import { UsersResponseDto } from "@dtos/users";
import { LinkInterface } from "@entities/link/interfaces/link.interface";

@Controller("links")
export class LinksController {
  constructor(
    private linkService: LinkService,
    private userService: UsersService
  ) {}

  @Get()
  async findAll(
    @Query("page") page: number,
    @Query("limit") limit: number
  ): Promise<{ post: LinkResponseDto; userInfo: UsersResponseDto }[]> {
    const posts = await this.linkService.getLinks(page, limit);
    let newResponse = [];

    for (let i = 0; i < posts.length; i++) {
      const user = await this.userService.getUserById(posts[i].userId);
      newResponse[i] = {
        post: new LinkResponseDto(posts[i] as LinkInterface),
        userInfo: new UsersResponseDto(user),
      };
    }

    return newResponse;
  }

  @Get("by-user-id")
  async findByUserId(
    @Query("userId") userId: number
  ): Promise<LinkResponseDto[]> {
    const posts = await this.linkService.getLinksByUserId(userId);
    return posts.map((post) => new LinkResponseDto(post));
  }

  @Get(":postId")
  async findOne(@Param("postId") id: number): Promise<LinkResponseDto> {
    const post = await this.linkService.getLinkById(id);

    if (post) {
      return new LinkResponseDto(post);
    }

    throw new NotFoundException();
  }

  @Post()
  @UseGuards(AuthGuard)
  @HttpCode(201)
  async create(@Body() data: LinkRequestDto): Promise<LinkResponseDto> {
    try {
      const post = await this.linkService.createLink(data);
      return new LinkResponseDto(post);
    } catch (error) {
      throw new BadRequestException();
    }
  }

  @Put()
  @UseGuards(AuthGuard)
  @HttpCode(204)
  async update(@Body() data: LinkRequestDto): Promise<void> {
    await this.linkService.updateLink(data);
  }

  @Delete(":linkId")
  @UseGuards(AuthGuard)
  @HttpCode(204)
  async delete(@Param("linkId") id: number): Promise<void> {
    const link = await this.linkService.getLinkById(id);

    await this.linkService.deleteLink(id);
  }
}
