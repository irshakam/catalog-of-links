import { Module } from '@nestjs/common';

import { LinkEntityModule } from '@entities/link/link.module';
import { UsersEntityModule } from '@entities/user/user.module';

import { LinksController } from './links.controller';

@Module({
    imports: [LinkEntityModule, UsersEntityModule],
    controllers: [LinksController],
})
export class LinksApiModule {}
