import {
  Get,
  Post,
  Body,
  Param,
  Query,
  Delete,
  HttpCode,
  Controller,
  ConflictException,
  BadRequestException,
  NotFoundException,
  Put,
  UseGuards,
} from "@nestjs/common";

import { UsersService } from "@entities/user/users.service";

import { UsersRequestDto, UsersResponseDto } from "@dtos/users";
import { AuthGuard } from "@common/guard/authGuard";

@Controller("users")
export class UsersController {
  constructor(private userService: UsersService) {}

  @Get()
  @UseGuards(AuthGuard)
  async findAll(
    @Query("page") page: number,
    @Query("limit") limit: number
  ): Promise<UsersResponseDto[]> {
    const users = await this.userService.getUsers(page, limit);
    return users.map((user) => new UsersResponseDto(user));
  }

  @Get(":userId")
  @UseGuards(AuthGuard)
  async findOne(@Param("userId") id: number): Promise<UsersResponseDto> {
    const user = await this.userService.getUserById(id);

    if (user) {
      return new UsersResponseDto(user);
    }

    throw new NotFoundException();
  }

  @Post()
  @HttpCode(201)
  async create(@Body() data: UsersRequestDto): Promise<UsersResponseDto> {
    try {
      const user = await this.userService.createUser(data);
      return new UsersResponseDto(user);
    } catch (error) {
      if (error.code === "ER_DUP_ENTRY") {
        throw new ConflictException("User with this email already exists");
      } else {
        throw new BadRequestException();
      }
    }
  }

  @Put()
  @UseGuards(AuthGuard)
  @HttpCode(204)
  async update(@Body() data: UsersRequestDto): Promise<void> {
    await this.userService.updateUser(data);
  }

  @Delete(":userId")
  @UseGuards(AuthGuard)
  @HttpCode(204)
  async delete(@Param("userId") id: number): Promise<void> {
    await this.userService.deleteUser(id);
  }
}
