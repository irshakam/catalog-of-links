import { Module } from "@nestjs/common";

import { UsersEntityModule } from "@entities/user/user.module";

import { AuthController } from "./auth.controller";

@Module({
  imports: [UsersEntityModule],
  controllers: [AuthController],
})
export class AuthApiModule {}
