import {
  Post,
  Body,
  Res,
  HttpCode,
  Controller,
  HttpStatus,
  BadRequestException,
} from "@nestjs/common";
import { v4 as uuidv4 } from "uuid";
import { Response } from "express";
import * as moment from "moment";

import { UsersService } from "@entities/user/users.service";

import { Tokens } from "@constants/token";

@Controller("login")
export class AuthController {
  constructor(private userService: UsersService) {}

  @Post()
  @HttpCode(201)
  async loginUser(
    @Body() data: { email: string; password: string },
    @Res() res: Response
  ): Promise<Response> {
    const user = await this.userService.isLogin(data.email, data.password);

    if (!!user) {
      const newToken = uuidv4();
      Tokens.push({ id: newToken, due: moment().add(60, "seconds").format() });
      return res.status(HttpStatus.OK).send({ token: newToken, user });
    } else throw new BadRequestException();
  }
}
