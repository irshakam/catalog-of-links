export let Tokens: TokenProps[] = [];

export interface TokenProps {
  id: string;
  due: string;
}

export const setNewTokensList = (newArr: TokenProps[]) => {
  Tokens = newArr;
};
