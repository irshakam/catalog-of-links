import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";

import { UserInterface } from "./interfaces/user.interface";

import { UserRepository } from "./user.repository";
import { Tokens } from "@constants/token";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository
  ) {}

  async getUsers(page = 1, size = 10): Promise<UserInterface[]> {
    return await this.userRepository.find({
      skip: (page - 1) * size,
      take: size,
    });
  }

  async getUserById(id: number): Promise<UserInterface> {
    return await this.userRepository.findOne({
      where: { id },
    });
  }

  async createUser(user: Partial<UserInterface>): Promise<UserInterface> {
    return await this.userRepository.save({
      name: user.name,
      password: user.password,
      email: user.email,
    });
  }

  async updateUser(user: Partial<UserInterface>): Promise<void> {
    await this.userRepository.update({ id: user.id }, user);
  }

  async getUserByEmail(email: string): Promise<UserInterface> {
    return await this.userRepository.findByEmail(email);
  }

  async deleteUser(id: number): Promise<void> {
    await this.userRepository.delete(id);
  }

  async isLogin(email: string, password: string): Promise<UserInterface> {
    return await this.userRepository.findOne({
      where: { email, password },
    });
  }

  isAuthorized(tokenId: string): boolean {
    return Tokens.some((tokenItem) => tokenItem.id === tokenId);
  }
}
