import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { LinkService } from "./link.service";
import { LinkRepository } from "./link.repository";

@Module({
  imports: [TypeOrmModule.forFeature([LinkRepository])],
  providers: [LinkService],
  exports: [LinkService],
})
export class LinkEntityModule {}
