import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  Unique,
} from "typeorm";

import { LinkInterface } from "./interfaces/link.interface";

@Entity()
@Unique("id", ["id"])
export class Link implements LinkInterface {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "integer", nullable: false, unique: false })
  userId: number;

  @Column({
    type: "varchar",
    length: 100,
    nullable: false,
    default: "",
    unique: false,
  })
  userName: string;

  @Column({ type: "longtext", nullable: false })
  link: string;

  @Column({ type: "longtext", nullable: false })
  about: string;

  @Column({ type: "longtext", nullable: false })
  tag: string;

  @Column({ type: "boolean", nullable: false, unique: false })
  isActive: boolean;

  @CreateDateColumn({ update: false })
  createdAt: Date;

  @UpdateDateColumn({ update: true })
  updatedAt: Date;
}
