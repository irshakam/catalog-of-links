import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";

import { LinkInterface } from "./interfaces/link.interface";

import { LinkRepository } from "./link.repository";

@Injectable()
export class LinkService {
  constructor(
    @InjectRepository(LinkRepository)
    private LinkRepository: LinkRepository
  ) {}

  async getLinks(page = 1, size = 10): Promise<LinkInterface[]> {
    return await this.LinkRepository.find({
      skip: (page - 1) * size,
      take: size,
    });
  }

  async getLinkById(id: number): Promise<LinkInterface> {
    return await this.LinkRepository.findOne({
      where: { id },
    });
  }

  async getLinksByUserId(userId: number): Promise<LinkInterface[]> {
    return await this.LinkRepository.find({
      where: { userId },
    });
  }

  async createLink(link: Partial<LinkInterface>): Promise<LinkInterface> {
    return await this.LinkRepository.save({
      userId: link.userId,
      userName: link.userName,
      link: link.link,
      about: link.about,
      tag: link.tag,
      isActive: link.isActive,
    });
  }

  async updateLink(link: Partial<LinkInterface>): Promise<void> {
    await this.LinkRepository.update({ id: link.id }, link);
  }

  async deleteLink(id: number): Promise<void> {
    await this.LinkRepository.delete(id);
  }
}
