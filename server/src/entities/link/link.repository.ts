import { EntityRepository, Repository } from "typeorm";

import { Link } from "./link";

@EntityRepository(Link)
export class LinkRepository extends Repository<Link> {}
