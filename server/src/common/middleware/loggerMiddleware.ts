import { Request, Response, NextFunction } from "express";
import { Injectable, NestMiddleware } from "@nestjs/common";
import { pushLogs } from "@helpers/logs";

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(request: Request, res: Response, next: NextFunction) {
    pushLogs(
      request.method,
      request.url,
      request.headers["user-agent"] as string,
      request.ip
    );
    next();
  }
}
