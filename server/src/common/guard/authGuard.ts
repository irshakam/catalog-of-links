import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { UsersService } from "@entities/user/users.service";
import { Observable } from "rxjs";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: UsersService) {}

  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.authService.isAuthorized(request.headers.authorization);
  }
}
