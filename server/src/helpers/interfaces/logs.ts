export interface Log {
  date: string;
  methodName: string;
  url: string;
  user: {
    agent: string;
    ip: string;
  };
}
