import * as fs from "fs";
import * as moment from "moment";
import { Log } from "./interfaces/logs";

export const pushLogs = (
  methodName: string,
  url: string,
  userAgent: string,
  userIP: string
) => {
  const log: Log = {
    date: moment().format(),
    methodName: methodName,
    url: url,
    user: {
      agent: userAgent,
      ip: userIP,
    },
  };

  fs.appendFile("log.txt", JSON.stringify(log) + "\n", (err) => {
    if (err) {
      console.log(err);
    }
  });
};
